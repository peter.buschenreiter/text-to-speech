package screenReader.model;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

public class ScreenReader {

	private final String voiceName;
	private String text;

	public ScreenReader() {
		System.setProperty("freetts.voices",
				"com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");
		voiceName = "kevin16";
	}

	public void readAloud() {
		Voice voice = VoiceManager.getInstance().getVoice(voiceName);
		voice.allocate();
		voice.speak(text);
		voice.deallocate();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
