package screenReader;

import screenReader.model.ScreenReader;
import screenReader.view.ScreenReaderPresenter;
import screenReader.view.ScreenReaderView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		ScreenReader model = new ScreenReader();
		ScreenReaderView view = new ScreenReaderView();

		ScreenReaderPresenter presenter = new ScreenReaderPresenter(model, view);
		primaryStage.setScene(new Scene(view));

		primaryStage.setResizable(false);
		primaryStage.setWidth(300);
		primaryStage.setHeight(450);
		primaryStage.setTitle("Screen Reader");

		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
