package screenReader.view;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;

public class ScreenReaderView extends BorderPane {
	private Button button;
	private TextArea textArea;

	public ScreenReaderView() {
		initializeNodes();
		layoutNodes();
	}

	private void initializeNodes() {
		button = new Button("Read me!");
		textArea = new TextArea("Example text");
	}

	private void layoutNodes() {
		setCenter(textArea);
		setBottom(button);

		setAlignment(button, Pos.CENTER);
		setMargin(button, new Insets(10));
	}

	// package-private getters for controls to use in Presenter class

	Button getButton() {
		return button;
	}

	TextArea getTextArea() {
		return textArea;
	}
}
