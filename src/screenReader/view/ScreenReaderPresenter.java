package screenReader.view;

import screenReader.model.ScreenReader;

public class ScreenReaderPresenter {
	private final ScreenReader model;
	private final ScreenReaderView view;

	public ScreenReaderPresenter(ScreenReader model, ScreenReaderView view) {
		this.model = model;
		this.view = view;

		model.setText(view.getTextArea().getText());

		addEventHandlers();
		updateView();
	}

	private void addEventHandlers() {
		view.getTextArea()
		    .textProperty()
		    .addListener((observableValue, oldValue, newValue) -> model.setText(newValue));

		view.getButton().setOnAction(actionEvent -> model.readAloud());
	}

	private void updateView() {
		// fills view
	}
}
